# [`dnscrypt-server`](https://git.dotya.ml/dotya.ml/dnscrypt-server)

this repo holds configuration files for dotya.ml's DNSCrypt installation.

## what exactly?
* containerised [`encrypted-dns`](https://github.com/DNSCrypt/encrypted-dns-server)
* [OpenNIC](https://www.opennic.org/) domain support
  * test using the awesome [`doggo`](https://github.com/mr-karan/doggo):
  ```shell
  doggo --debug --json NS epic. @sdns://AQcAAAAAAAAAETE0NC45MS43MC42Mjo1NDQzIHF-JiN46cNwFXJleEVWGWgrhe2QeysUtZoo9HwzYCMzITIuZG5zY3J5cHQtY2VydC5kbnNjcnlwdC5kb3R5YS5tbA
  ```
  * example response:
  ```shell
  DEBUG[2022-09-01T00:22:23+02:00] initiating DNSCrypt resolver

  DEBUG[2022-09-01T00:22:23+02:00] Starting doggo 🐶

  DEBUG[2022-09-01T00:22:23+02:00] Attempting to resolve domain=epic. nameserver="144.91.70.62:5443" ndots=0
  [
      {
          "answers": [
              {
                  "name": "epic.",
                  "type": "NS",
                  "class": "IN",
                  "ttl": "86400s",
                  "address": "ns13.opennic.glue.",
                  "status": "",
                  "rtt": "45ms",
                  "nameserver": "144.91.70.62:5443"
              }
          ],
          "authorities": null,
          "questions": [
              {
                  "name": "epic.",
                  "type": "NS",
                  "class": "IN"
              }
          ]
      }
  ]
  ```

a short asciicast of `doggo` interacting with our server:
[![asciicast](https://asciinema.org/a/KPGuJaYiztnOmCzCSwtfExpB3.svg)](https://asciinema.org/a/KPGuJaYiztnOmCzCSwtfExpB3)

## why though
* improved DNS security: DNSSEC-validated responses protected by [`DNSCrypt`](https://github.com/DNSCrypt/dnscrypt-protocol/blob/master/DNSCRYPT-V2-PROTOCOL.txt)
* support for [`Anonymized DNSCrypt`](https://github.com/DNSCrypt/dnscrypt-protocol/blob/master/ANONYMIZED-DNSCRYPT.txt)
* DNS neutrality: *moar DNS == moar better*
* no logging: increased privacy
* easy access to OpenNIC interwebz
* self-hosting is fun

## observability
a dashboard ([source](https://github.com/DNSCrypt/encrypted-dns-server/issues/66#issuecomment-868753382))
is available for conveniently presented performance insights and cache
efficiency monitoring, deployed at https://grafana.dotya.ml/d/kX2luvMnz/dnscrypt


## TO DO
- [ ] automated deployment (preferably using `ansible` + `drone`)

## LICENSE
WTFPLv2, see [LICENSE](LICENSE) for details.
